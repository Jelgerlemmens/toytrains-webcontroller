package org.lem.toytrains;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Base64;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import org.junit.jupiter.api.Test;

class SerialTest {
	
	@Test
	public void serial(){
		SerialPort com6 = SerialPort.getCommPort("COM6");
		com6.addDataListener(new SerialPortDataListener() {
			@Override
			public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; }
			@Override
			public void serialEvent(SerialPortEvent event)
			{
				if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
					return;
				byte[] newData = new byte[com6.bytesAvailable()];
				int numRead = com6.readBytes(newData, newData.length);
				System.out.println("Read " + numRead + " bytes.");
				String s = new String(newData);
				System.out.println(s);
			}
		});
		com6.openPort();
		String hiya = "<1>";
		com6.writeBytes(hiya.getBytes(),hiya.getBytes().length);
		com6.closePort();
	}
}
package org.lem.toytrains.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;

import org.junit.jupiter.api.Test;
import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.Turnout;

class PersistTrackUtilTest {

	public String saveFilePath = System.getProperty("user.home")+ File.separator+"tracksave.json";;
	
	@Test
	public void persistTrack() throws Exception {
		Track track = new Track();
		Turnout turnout_0 = new Turnout(1,0);
		Turnout turnout_1 = new Turnout(1,1);
		Cab cab = new Cab(27);
		track.add(cab);
		track.add(turnout_0);
		track.add(turnout_1);

		PersistTrackUtil.persistTrack(track, saveFilePath);

		Track track1 = null;
		try {
			track1 = PersistTrackUtil.loadTrack(saveFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertThat(track.getTurnouts().size()).isEqualTo(track1.getTurnouts().size());
		assertThat(track.getCabs().get(27).getAddress()).isEqualTo(track1.getCabs().get(27).getAddress());
	}
	
}
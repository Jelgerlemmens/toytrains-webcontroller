package org.lem.toytrains.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.lem.toytrains.util.os.OSType;
import org.lem.toytrains.util.os.OSTypeFinder;

class OSTypeFinderTest {
	
	@Test
	public void getOsType(){
		OSType currentOsType = null;
		try {
			currentOsType = OSTypeFinder.getCurrentOsType();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String s = System.getProperty("os.name").toLowerCase();
		assertThat(currentOsType).isNotNull();
		assertThat(s).contains(currentOsType.toString());
	}
}
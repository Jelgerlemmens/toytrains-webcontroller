package org.lem.toytrains.util;

import static org.junit.jupiter.api.Assertions.*;

import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.Turnout;


class TrackLogicUtilTest {
	
	@org.junit.jupiter.api.Test
	void switchTurnout() {
		Turnout t = new Turnout();
		t.setTurn(true);
		assertFalse(TrackLogicUtil.switchTurnout(t));
		t.setTurn(false);
		assertTrue(TrackLogicUtil.switchTurnout(t));
	}
	
	@org.junit.jupiter.api.Test
	void toggleTrackPower() {
		Track t = new Track();
		t.setPower(true);
		assertFalse(TrackLogicUtil.toggleTrackPower(t));
		t.setPower(false);
		assertTrue(TrackLogicUtil.toggleTrackPower(t));
	}
	
	@org.junit.jupiter.api.Test
	void getCabDirection() {
		int rawSpeedVal = -78;
		assertEquals(1, TrackLogicUtil.getCabDirection(rawSpeedVal));
		rawSpeedVal = 45;
		assertEquals(0, TrackLogicUtil.getCabDirection(rawSpeedVal));
	}
	
	@org.junit.jupiter.api.Test
	void getCabSpeed() {
		int rawSpeedVal = 78; //forward
		assertEquals(78,TrackLogicUtil.getCabSpeed(rawSpeedVal));
		rawSpeedVal = -78; //reverse (direction should be switched)
		assertEquals(78,TrackLogicUtil.getCabSpeed(rawSpeedVal));
		rawSpeedVal = 0; // full stop
		assertEquals(-1,TrackLogicUtil.getCabSpeed(rawSpeedVal));
	}
	
	@org.junit.jupiter.api.Test
	void getCabLights() {
		Cab cab = new Cab();
		cab.setLights(true);
		assertFalse(TrackLogicUtil.getCabLights(cab));
		cab.setLights(false);
		assertTrue(TrackLogicUtil.getCabLights(cab));
	}
	
	@org.junit.jupiter.api.Test
	void getCabSound() {
		Cab cab = new Cab();
		cab.setSound(true);
		assertFalse(TrackLogicUtil.getCabSound(cab));
		cab.setSound(false);
		assertTrue(TrackLogicUtil.getCabSound(cab));
	}
}
package org.lem.toytrains.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TrackTest {

	
	@Test
	public void turnoutAddTest(){
		final int TURNOUT_PRIMARY_ADDRESS = 1;
		Track track = new Track();
		Turnout turnout_1 = new Turnout(TURNOUT_PRIMARY_ADDRESS,1);
		Turnout turnout_2 = new Turnout(TURNOUT_PRIMARY_ADDRESS,2);
		
		track.add(turnout_1);
		track.add(turnout_2);
		
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).size()).isEqualTo(2);
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).get(0).getSubAddress()).isNotEqualTo(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).get(1).getSubAddress());
	}
	
	@Test
	public void turnOutRemoveSucceedTest(){
		final int TURNOUT_PRIMARY_ADDRESS = 1;
		Track track = new Track();
		Turnout turnout_1 = new Turnout(TURNOUT_PRIMARY_ADDRESS,1);
		Turnout turnout_2 = new Turnout(TURNOUT_PRIMARY_ADDRESS,2);
		
		track.add(turnout_1);
		track.add(turnout_2);
		
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).size()).isEqualTo(2);
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).get(0).getSubAddress()).isNotEqualTo(
			track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).get(1).getSubAddress());
		
		Turnout turnout_new_obj = new Turnout(1,1);
		try {
			track.remove(turnout_new_obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).size()).isEqualTo(1);
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).get(0).getSubAddress()).isEqualTo(2);
	}
	
	@Test
	public void turnOutRemoveFailTest(){
		final int TURNOUT_PRIMARY_ADDRESS = 1;
		final int TURNOUT_PRIMARY_FAIL_ADDRESS = 4;
		Track track = new Track();
		Turnout turnout_1 = new Turnout(TURNOUT_PRIMARY_ADDRESS,0);
		Turnout turnout_2 = new Turnout(TURNOUT_PRIMARY_ADDRESS,1);
		Turnout turnout_3 = new Turnout(TURNOUT_PRIMARY_ADDRESS,2);
		
		track.add(turnout_1);
		track.add(turnout_2);
		track.add(turnout_3);
		
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).size()).isEqualTo(3);
		assertThat(track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).get(0).getSubAddress()).isNotEqualTo(
			track.getTurnouts().get(TURNOUT_PRIMARY_ADDRESS).get(1).getSubAddress());
		
		//fail on primary address, does not exist in map
		Turnout turnout_new_obj = new Turnout(TURNOUT_PRIMARY_FAIL_ADDRESS,1);
		Exception exception = assertThrows(Exception.class, () -> {
			track.remove(turnout_new_obj);
		});
		
		//fail on sub address range from 0-3, and also does not occur
		Turnout turnout_new_obj_0 = new Turnout(TURNOUT_PRIMARY_ADDRESS,5);
		Exception exception_0 = assertThrows(Exception.class, () -> {
			track.remove(turnout_new_obj_0);
		});
		
		assertThat(exception.getMessage()).isEqualTo("Turnout primary address does not exist");
		assertThat(exception_0.getMessage()).isEqualTo("Turnout sub address does not exist");
	}
	
}
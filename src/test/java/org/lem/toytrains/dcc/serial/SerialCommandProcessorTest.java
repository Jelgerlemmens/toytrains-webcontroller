package org.lem.toytrains.dcc.serial;

import static org.assertj.core.api.Assertions.assertThat;

import org.lem.toytrains.domain.Turnout;
import org.junit.jupiter.api.Test;

class SerialCommandProcessorTest {
	
	
	
	@Test
	public void testProcessTurnout(){
		final int TURNOUT_ADDRESS = 1;
		final int TURNOUT_SUB_ADDRESS = 1;
		SerialCommandProcessor serialCommandProcessor = new SerialCommandProcessor();
		SerialCommandBuilder serialCommandBuilder = new SerialCommandBuilder();
		Turnout turnout = new Turnout(TURNOUT_ADDRESS,TURNOUT_SUB_ADDRESS);
		turnout.setTurn(true);
		
		SerialCommand serialCommand = serialCommandBuilder
			.withTurnout(turnout)
			.withSerialCommandEvent(SerialCommandEvent.TURNOUT)
			.build();
		
		String processed = serialCommandProcessor.process(serialCommand);
		final String expected = "<a 01 01 01>";
		
		assertThat(processed.equals(expected));
		
	}
	
}
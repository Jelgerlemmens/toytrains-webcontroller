package org.lem.toytrains.controller.entity;

import org.lem.toytrains.domain.MultiPlayGame;
import org.lem.toytrains.domain.Player;
import org.lem.toytrains.domain.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiPlayGameFactory {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TrackFactory.class);
	private static MultiPlayGame MULTI_PLAYER_GAME;
	
	/**
	 * A multiplayer game can exist or not exist, but there can never be more than one instance of a game.
	 * There is only one track with a finite number of cabs, every cab can reach every switch.
	 *
	 * @param track
	 * @param admin
	 * @return
	 */
	
	public static MultiPlayGame startNewMultiPlayGame(Track track, Player admin){
		LOGGER.info("Starting new multiplayer game! Admin is: {} Track details: {}",admin.toString(),track.toString());
		MULTI_PLAYER_GAME = new MultiPlayGame(track,admin);
		return MULTI_PLAYER_GAME;
	}
	
	public static void endMultiPlayGame(){
		LOGGER.info("Ending multiplayer game!");
		TrackFactory.eraseAndRecreateTrack();
		MULTI_PLAYER_GAME = null;
	}
	
	public static MultiPlayGame getMultiPlayerGame(){
		return MULTI_PLAYER_GAME;
	}
	
}

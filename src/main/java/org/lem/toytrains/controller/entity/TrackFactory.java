package org.lem.toytrains.controller.entity;

import java.util.Objects;

import org.lem.toytrains.domain.Track;
import org.lem.toytrains.util.PersistTrackUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrackFactory {
	
	private static Track TRACK;
	private static final Logger LOGGER = LoggerFactory.getLogger(TrackFactory.class);
	private static final String TRACK_NAME = "TheTrack";
	
	/**
	 * A single track exists within the controller, this is a representation of whatever track layout is made in real life.
	 * The track may hold any amount of locomotives (cabs) and any amount of turnouts (rail switches) These can be dynamically added and removed
	 * from the track. The control over these cabs & turnouts is via a standard (simple) dcc address.
	 * For cabs a dcc++ base station cab address is used
	 * For turnouts a dcc++ base station accessory address is used.
	 *
	 * @return
	 */
	
	public static Track getTrack(){
		if(Objects.isNull(TRACK)){
			try{
				TRACK = PersistTrackUtil.loadTrack(PersistTrackUtil.SAVE_FILE_PATH);
			}catch (Exception e){
				LOGGER.info("No track save file found, building new track...");
				TRACK = new Track(TRACK_NAME);
			}
		}
		return TRACK;
	}
	
	public static void eraseAndRecreateTrack(){
		TRACK = null;
		TRACK = new Track(TRACK_NAME);
		LOGGER.info("Track has been erased and recreated!");
	}
}

package org.lem.toytrains.controller;

import java.util.List;

import org.lem.toytrains.dcc.DccEventHandler;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.Turnout;
import org.lem.toytrains.util.TrackLogicUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TurnoutOperationsRESTController {
	
	@Autowired
	Track track;
	
	@Autowired
	DccEventHandler dccEventHandler;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CabOperationsRESTController.class);
	
	
	
	@PostMapping("/addturnout")
	public Turnout addturnout(@RequestBody Turnout turnout) {
		Turnout addedTurnout = track.add(turnout);;
		LOGGER.debug("Added turnout: {} - {}", turnout.getAddress(), turnout.getSubAddress());
		return addedTurnout;
	}
	
	@PostMapping("/deleteturnout")
	public Turnout deleteturnout(@RequestBody Turnout turnout) {
		Turnout deletedTurnout = null;
		try{
			deletedTurnout = track.remove(turnout);
			LOGGER.debug("Deleted turnout: {} - {}", turnout.getAddress(), turnout.getSubAddress());
		}catch(Exception e){
			LOGGER.error("Could not delete tunrout: {} - {}", turnout.getAddress(), turnout.getSubAddress());
			e.printStackTrace();
		}
		return deletedTurnout;
	}
	
	@PostMapping("/toggleturnout")
	public void toggleTurnout(@RequestBody Turnout turnout) {
		List<Turnout> turnouts = track.getTurnouts().get(turnout.getAddress());
		turnouts.forEach(turnoutInTrack -> {
			if(turnoutInTrack.getSubAddress() == turnout.getSubAddress()){
				turnoutInTrack.setTurn(TrackLogicUtil.switchTurnout(turnoutInTrack));
				dccEventHandler.handle(turnoutInTrack);
				LOGGER.debug("Toggled turnout: {} - {}", turnout.getAddress(), turnout.getSubAddress());
			}
		});
		
	}
}

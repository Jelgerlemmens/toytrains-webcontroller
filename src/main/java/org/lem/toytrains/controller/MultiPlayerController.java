package org.lem.toytrains.controller;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.lem.toytrains.domain.Player;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.controller.entity.MultiPlayGameFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MultiPlayerController {
	
	@Autowired
	private Track track;
	
	@Autowired
	public SessionRegistry sessionRegistry;
	
	public static final String MULTIPLAY_LOBBY = "multiplayer_lobby";
	public static final String ADMIN_VIEW = "multiplayer_admin_view";
	public static final String PLAYER_VIEW = "multiplayer_player_view";
	
	@GetMapping("/multiplayer")
	public String trackopsMulti(Model model, HttpSession session) {
		//check if a player already exists for the current sessionId
		Player player = getPlayerForSessionId(session);
		if(Objects.nonNull(player)){
			//apparently this player already exists in our registry.. could be that he missed login out or something?
			if(player.isAdmin()){
				//well crap.. it's apparently the admin who managed to loose his trackoverview.. let's redirect him there
				return "redirect:adminplayerview";
			}else{
				// alost player perhaps?
				return "redirect:playerview";
			}

		}
		model.addAttribute("multiPlayGame", MultiPlayGameFactory.getMultiPlayerGame());
		model.addAttribute("track", track);
		return MULTIPLAY_LOBBY;
	}
	
	//admin has the trackops view, can control all
	@GetMapping("/adminplayerview")
	public String adminPlayerView(Model model, HttpSession session) throws Exception {
		Player player = getPlayerForSessionId(session);
		if(Objects.nonNull(player)){
			if(player.isAdmin()){
				model.addAttribute("track", track);
				model.addAttribute("admin", getPlayerForSessionId(session));
				model.addAttribute("game", MultiPlayGameFactory.getMultiPlayerGame());
				return ADMIN_VIEW;
			}
		}
		throw new Exception("YOU ARE NOT ADMIN!");
	}
	
	//when going to playerview, the player exists, has a linked cab n all that
	@GetMapping("/playerview")
	public String playerView(Model model, HttpSession session) throws Exception {
		//if there is a valid game in progress
		if(Objects.nonNull(MultiPlayGameFactory.getMultiPlayerGame())){
			//when going to the playerView the player should be registered in the game and in the sessionReg
			if(Objects.nonNull(MultiPlayGameFactory.getMultiPlayerGame().getPlayer(session.getId())) &&
				Objects.nonNull(getPlayerForSessionId(session))){
				model.addAttribute("player", getPlayerForSessionId(session));
				model.addAttribute("turnouts", track.getTurnouts());
				return PLAYER_VIEW;
			}
			throw new Exception("NOT A VALID PLAYER!");
		}else{
			//no game available, go to home
			return "redirect:index";
		}
	}

	private Player getPlayerForSessionId(HttpSession session) {
		Player player = null;
		String sessionId = session.getId();
		SessionInformation sessionInformation = sessionRegistry.getSessionInformation(sessionId);
		if (Objects.nonNull(sessionInformation) && Objects.nonNull(MultiPlayGameFactory.getMultiPlayerGame())) {
			player = MultiPlayGameFactory.getMultiPlayerGame().getPlayer(sessionId);
		}
		return player;
	}
	
}

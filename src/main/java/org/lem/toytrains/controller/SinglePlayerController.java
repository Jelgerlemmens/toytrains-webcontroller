package org.lem.toytrains.controller;

import org.lem.toytrains.domain.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SinglePlayerController {
	
	public static final String SINGLEPLAYER = "singleplayer";
	
	@Autowired
	Track track;
	
	@GetMapping("/singleplayer")
	public String singlePlayerOps(Model model) {
		model.addAttribute("track", track);
		return SINGLEPLAYER;
	}
}

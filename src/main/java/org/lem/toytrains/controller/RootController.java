package org.lem.toytrains.controller;

import org.lem.toytrains.controller.entity.MultiPlayGameFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class RootController {
	
	private static final String ROOT = "index";
	
	@GetMapping("/")
	public ModelAndView rootCall(ModelMap model) {
		model.addAttribute("multiplaygame", MultiPlayGameFactory.getMultiPlayerGame());
		return new ModelAndView(ROOT, model);
	}
	
	@GetMapping("/tosingleplay")
	public String toSinglePlay() {
		return "redirect:singleplayer";
	}
	
	@GetMapping("/tomultiplay")
	public String toMultiPlay() {
		return "redirect:multiplayer";
	}
	
}

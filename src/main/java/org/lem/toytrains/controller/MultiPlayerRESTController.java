package org.lem.toytrains.controller;

import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.lem.toytrains.dcc.DccEventHandler;
import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.MultiPlayGame;
import org.lem.toytrains.domain.Player;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.TrackEvent;
import org.lem.toytrains.controller.entity.MultiPlayGameFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
	A controller controlling multiplayer games
 */
@RestController
public class MultiPlayerRESTController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MultiPlayerRESTController.class);
	
	@Autowired
	public SessionRegistry sessionRegistry;
	
	@Autowired
	Track track;
	
	@Autowired
	DccEventHandler dccEventHandler;
	
	@GetMapping("/newmultiplayergame")
	public MultiPlayGame startNewMultiPlayerGame(@RequestParam String adminPlayerName, HttpSession session){
		MultiPlayGame game = null;
		if(Objects.isNull(MultiPlayGameFactory.getMultiPlayerGame())){
			String sessionId = session.getId();
			SessionInformation sessionInformation = sessionRegistry.getSessionInformation(sessionId);
			if(Objects.isNull(sessionInformation)){
				sessionRegistry.registerNewSession(sessionId, Player.PLAYER_PRINCIPAL);
				sessionInformation = sessionRegistry.getSessionInformation(sessionId);
			}
			Player adminPlayer = new Player(adminPlayerName,sessionInformation,true);
			adminPlayer.setSessionId(sessionInformation.getSessionId());
			game = MultiPlayGameFactory.startNewMultiPlayGame(track,adminPlayer);
			LOGGER.info("New multiplayer game created! Admin user is: {} with sessionId: {}",adminPlayer.getPlayerName(), adminPlayer.getSessionId());
			return game;
			
		}
		LOGGER.info("Returning existing game, cannot create new game while game still in progress, admin should kill game first!");
		game =  MultiPlayGameFactory.getMultiPlayerGame();
		return game;
	}
	
	@GetMapping("/endmultiplayergame")
	public boolean endMultiplayerGame(@RequestParam String adminPlayerName, @RequestParam String sessionId) throws Exception {
		//kills the game (to null) and recreates the track, erasing all cabs and players
		Player admin = getPlayerForSessionId(sessionId);
		if(admin.isAdmin() && admin.getPlayerName().equals(adminPlayerName)){
			//end the game
			MultiPlayGameFactory.endMultiPlayGame();
			//clear session registry
			clearAllSessions();
			//shutdown track power
			this.track.setTrackEvent(TrackEvent.POWER);
			this.track.setPower(false);
			dccEventHandler.handle(this.track);
			LOGGER.info("Admin has stopped the multiplayer game");
			return true;
		}
		throw new Exception("GAME COULD NOT BE STOPPED, CRITICAL ERROR");
		
	}
	
	
	@PostMapping("/joinmultiplayergame")
	public Player joinMultiPlayerGame(@RequestBody Player player, HttpSession session) throws Exception {
		//this specific user has not yet received a sessionId,
		Player fromRegistry = getPlayerForSessionId(session.getId());
		if(Objects.isNull(fromRegistry)){
			String sessionId = session.getId(); //the newly created session id, happens even if user does not have a sessionId yet
			sessionRegistry.registerNewSession(sessionId,Player.PLAYER_PRINCIPAL);
			SessionInformation sessionInformation = sessionRegistry.getSessionInformation(sessionId);
			player.setSessionInformation(sessionInformation);
			player.setSessionId(sessionId);
		}else{
			//apparently this player already exists and somehow ended up trying to log in again, we are going to return the player as is
			player = MultiPlayGameFactory.getMultiPlayerGame().getPlayer(fromRegistry.getSessionId());
			//perform a couple of checks to make sure everything is ok
			if(!player.getCab().isUsedInMultiPlay()){
				player.getCab().setUsedInMultiPlay(true);
			}
			LOGGER.info("Player: {} has re-joined multiplayer game",player.toString());
			return player;
		}
		if(Objects.nonNull(MultiPlayGameFactory.getMultiPlayerGame())){
			//add the player to the game.
			MultiPlayGameFactory.getMultiPlayerGame().addPlayer(player);
			//bind the available cab on the track to the player, and inform the cab to who it has been bound
			track.getCabs().get(player.getCab().getAddress()).setUsedInMultiPlay(true);
			track.getCabs().get(player.getCab().getAddress()).setImageName(player.getCab().getImageName());
			track.getCabs().get(player.getCab().getAddress()).setPlayerSessionId(player.getSessionId());
		}else{
			throw new Exception("NO GAME IS STARTED!");
		}
		LOGGER.info("Player: {} has joined multiplayer game",player.toString());
		return player;
	}
	
	
	@GetMapping("/logoutplayer")
	public boolean logoutPlayer(@RequestParam String playerName, @RequestParam String sessionId){
			Player player = getPlayerForSessionId(sessionId);
			//remove player from game
			MultiPlayGameFactory.getMultiPlayerGame().removePlayer(player);
			//remove player from sessionreg
			sessionRegistry.removeSessionInformation(sessionId);
			
			//unbind cab
			Cab cab = player.getCab();
			track.getCabs().get(cab.getAddress()).setPlayerSessionId(null);
			track.getCabs().get(cab.getAddress()).setImageName(null);
			track.getCabs().get(cab.getAddress()).setUsedInMultiPlay(false);
			LOGGER.info("Player: {} has left multiplayer game",player.toString());
			return true;
	}
	
	private Player getPlayerForSessionId(String sessionId) {
		Player player = null;
		SessionInformation sessionInformation = sessionRegistry.getSessionInformation(sessionId);
		if (Objects.nonNull(sessionInformation) && Objects.nonNull(MultiPlayGameFactory.getMultiPlayerGame())) {
			player = MultiPlayGameFactory.getMultiPlayerGame().getPlayer(sessionId);
		}
		return player;
	}
	
	private void clearAllSessions(){
		List<SessionInformation> sessionList = sessionRegistry.getAllSessions(Player.PLAYER_PRINCIPAL, true);
		for(SessionInformation sessionInformation : sessionList){
			sessionRegistry.removeSessionInformation(sessionInformation.getSessionId());
		}
	}
}


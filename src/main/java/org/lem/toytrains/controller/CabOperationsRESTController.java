package org.lem.toytrains.controller;

import java.util.ArrayList;
import java.util.List;

import org.lem.toytrains.dcc.DccEventHandler;
import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.CabEvent;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.util.TrackLogicUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CabOperationsRESTController {
	
	@Autowired
	Track track;
	
	@Autowired
	DccEventHandler dccEventHandler;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CabOperationsRESTController.class);
	
	@PostMapping(value = "/addcab")
	public Cab addcab(@RequestBody Cab cab) {
		Cab addedCab = track.add(cab);
		LOGGER.debug("Added cab: {}", addedCab.toString());
		return addedCab;
	}
	
	//returns the deleted cab, so frontend can delete the cab
	@PostMapping("/deletecab")
	public Cab deletecab(@RequestBody Cab cab) {
		Cab deletedCab = null;
		try{
			deletedCab = track.remove(cab);
		}catch(Exception e){
			LOGGER.error("Could not delete cab: {}", cab.getAddress());
			e.printStackTrace();
		}
		LOGGER.debug("Deleted cab: {}", deletedCab.toString());
		return deletedCab;
	}
	
	@PostMapping("/setcabspeed")
	public void setCabThrottle(@RequestBody Cab cab){
		Cab cabOnTrack = track.getCabs().get(cab.getAddress());
		cabOnTrack.setCabEvent(CabEvent.SPEED);
		cabOnTrack.setDirection(TrackLogicUtil.getCabDirection(cab.getSpeed()));
		cabOnTrack.setSpeed(TrackLogicUtil.getCabSpeed(cab.getSpeed()));
		dccEventHandler.handle(cabOnTrack);
		LOGGER.debug("Cab {} speed changed to {}", cabOnTrack.getAddress(), cabOnTrack.getSpeed());
	}
	
	@PostMapping("/setcablights")
	public Cab setCabLights(@RequestBody Cab cab){
		Cab cabOnTrack = track.getCabs().get(cab.getAddress());
		cabOnTrack.setCabEvent(CabEvent.LIGHTS);
		cabOnTrack.setLights(TrackLogicUtil.getCabLights(cabOnTrack));
		dccEventHandler.handle(cabOnTrack);
		LOGGER.debug("Cab {} lights changed to {}", cabOnTrack.getAddress(), cabOnTrack.isLights());
		return cabOnTrack;
	}
	
	@PostMapping("/setcabsound")
	public void setCabSound(@RequestBody Cab cab){
		Cab cabOnTrack = track.getCabs().get(cab.getAddress());
		cabOnTrack.setCabEvent(CabEvent.SOUND);
		cabOnTrack.setSound(TrackLogicUtil.getCabSound(cabOnTrack));
		dccEventHandler.handle(cabOnTrack);
		LOGGER.debug("Cab {} sound changed to {}", cabOnTrack.getAddress(), cabOnTrack.isSound());
	}
	
	//this should include the new id of the cab, now this is statically done in serialcommand to processed
	@GetMapping("/setcabaddress")
	public Cab setCabAddress(@RequestParam int oldCabId, @RequestParam int newCabId){
		Cab cabOnTrack = track.getCabs().get(oldCabId);
		cabOnTrack.setNewDccAddress(newCabId);
		cabOnTrack.setCabEvent(CabEvent.CHANGE_ADDRESS);
		dccEventHandler.handle(cabOnTrack);
		LOGGER.debug("Cab id {} changed to {}", cabOnTrack.getAddress(), newCabId);
		try{
			Cab deletedCab = track.remove(cabOnTrack);
			deletedCab.setAddress(newCabId);
			track.add(deletedCab);
			cabOnTrack = deletedCab;
		}catch(Exception e){
			LOGGER.error("error handling cab address update");
			e.printStackTrace();
		}
		return cabOnTrack;
	}
	
	@GetMapping("/allcabsontrack")
	public List<Cab> getAllCabsOnTrack(){
		return new ArrayList<>(track
			.getCabs()
			.values());
	}
}

package org.lem.toytrains.controller;

import org.lem.toytrains.dcc.DccEventHandler;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.TrackEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SingePlayerRESTController {
	
	@Autowired
	Track track;
	
	@Autowired
	DccEventHandler dccEventHandler;
	
	@GetMapping("/endtrackcontrol")
	public boolean endTrackControl(){
		this.track.setTrackEvent(TrackEvent.POWER);
		this.track.setPower(false);
		dccEventHandler.handle(this.track);
		return true;
	}
}

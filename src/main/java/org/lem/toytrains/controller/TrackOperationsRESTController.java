package org.lem.toytrains.controller;

import org.lem.toytrains.dcc.DccEventHandler;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.TrackEvent;
import org.lem.toytrains.util.PersistTrackUtil;
import org.lem.toytrains.util.TrackLogicUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrackOperationsRESTController {
	
	@Autowired
	Track track;
	
	@Autowired
	DccEventHandler dccEventHandler;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CabOperationsRESTController.class);
	
	@GetMapping("/savetrack")
	public boolean saveTrack(){
		if(PersistTrackUtil.persistTrack(this.track,PersistTrackUtil.SAVE_FILE_PATH)){
			LOGGER.info("Track saved!");
			return true;
		}
		return false;
	}
	
	@GetMapping("/toggletrackpower")
	public boolean toggleTrackPower(){
		this.track.setTrackEvent(TrackEvent.POWER);
		this.track.setPower(TrackLogicUtil.toggleTrackPower(this.track));
		dccEventHandler.handle(this.track);
		LOGGER.info("Track power is: {}",this.track.isPower());
		return this.track.isPower();
	}
}

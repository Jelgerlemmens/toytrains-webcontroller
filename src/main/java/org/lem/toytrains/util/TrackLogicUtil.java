package org.lem.toytrains.util;

import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.Turnout;

public class TrackLogicUtil {
	
	private TrackLogicUtil(){}

	
	public static boolean switchTurnout(Turnout t) {
		return !t.isTurn();
	}
	
	public static boolean toggleTrackPower(Track track){
		return !track.isPower();
	}
	
	public static int getCabDirection(int rawSpeedVal) {
		if(rawSpeedVal >= 0) {
			return 0;
		}else {
			return 1;
		}
	}
	
	public static int getCabSpeed(int rawSpeedVal) {
		if(rawSpeedVal == 0) {
			return -1;
		}else if(rawSpeedVal > 0) {
			return rawSpeedVal;
		}else if(rawSpeedVal < 0){
			return rawSpeedVal*-1;
		}
		return 1;
	}
	
	public static boolean getCabLights(Cab cab) {
		return !cab.isLights();
	}
	
	public static boolean getCabSound(Cab cab) {
		return !cab.isSound();
	}
}



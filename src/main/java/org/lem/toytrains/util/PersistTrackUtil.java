package org.lem.toytrains.util;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.lem.toytrains.domain.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersistTrackUtil {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersistTrackUtil.class);
	
	public static final String SAVE_FILE_PATH = System.getProperty("user.home")+File.separator+"tracksave.json";
	
	public static boolean persistTrack(Track track, String filePath) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(filePath), track);
			LOGGER.debug("Track saved on location {}", SAVE_FILE_PATH);
			return true;
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	//load the track, or throw an error
	public static Track loadTrack(String filePath) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		Track track =  objectMapper.readValue(new File(filePath), Track.class);
		track.setPower(false); // if a track gets loaded always turn power off. Arduino does not have a save state of the power on reboot.
		LOGGER.info("Loaded track form file: {}",track.toString());
		return track;
	}
}

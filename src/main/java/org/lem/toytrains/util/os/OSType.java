package org.lem.toytrains.util.os;

public enum OSType {
	OS_LINUX("linux"),
	OS_WINDOWS("windows");
	
	private final String osTypeName;
	
	OSType(String osTypeName) {this.osTypeName = osTypeName;}
	
	public String toString(){
		return this.osTypeName;
	}
	
}

package org.lem.toytrains.util.os;

import static org.lem.toytrains.util.os.OSType.OS_LINUX;
import static org.lem.toytrains.util.os.OSType.OS_WINDOWS;

import org.lem.toytrains.config.SerialConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OSTypeFinder {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SerialConfiguration.class);
	
	public static OSType getCurrentOsType() throws Exception {
		String os = System.getProperty("os.name").toLowerCase();
		LOGGER.debug("OS for serial init is: {} ",os);
		if(os.equals(OS_LINUX.toString())){
			return OS_LINUX;
		}
		if(os.contains(OS_WINDOWS.toString())){
			return OS_WINDOWS;
		}
		LOGGER.debug("Current OS not supported!: {} ",os);
		throw new Exception("Current OS not supported!");
	}
	
}

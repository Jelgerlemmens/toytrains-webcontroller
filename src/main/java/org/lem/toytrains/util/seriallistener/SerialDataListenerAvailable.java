package org.lem.toytrains.util.seriallistener;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListenerWithExceptions;
import com.fazecast.jSerialComm.SerialPortEvent;
import org.lem.toytrains.config.SerialConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerialDataListenerAvailable implements SerialPortDataListenerWithExceptions {
	
	private SerialPort commPort;
	private final Logger LOGGER = LoggerFactory.getLogger(SerialConfiguration.class);
	
	public SerialDataListenerAvailable(SerialPort commPort){
		this.commPort = commPort;
	}
	
	@Override
	public int getListeningEvents() {
		return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
	}
	
	@Override
	public void serialEvent(SerialPortEvent serialPortEvent) {
		if (serialPortEvent.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
			return;
		byte[] newData = new byte[commPort.bytesAvailable()];
		commPort.readBytes(newData, newData.length);
		LOGGER.debug(new String(newData));
	}
	
	@Override
	public void catchException(Exception e) {
		LOGGER.error("Serial error occured");
		e.printStackTrace();
	}
}

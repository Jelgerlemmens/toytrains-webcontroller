package org.lem.toytrains.config;

import com.fazecast.jSerialComm.SerialPort;
import org.lem.toytrains.util.seriallistener.SerialDataListenerAvailable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SerialConfiguration {
	
	@Value("${uart}")
	public String UART;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SerialConfiguration.class);
	public static int BAUD_RATE_115200 = 115200;
	public static int DATA_BITS_8 = 8;
	public static int STOP_BITS_1 = 1;
	public static int PARITY_NONE  = 0;
	
	@Bean
	public SerialPort serial(){
		SerialPort commPort = SerialPort.getCommPort(UART);
		commPort.setComPortParameters(BAUD_RATE_115200,DATA_BITS_8,STOP_BITS_1,PARITY_NONE);
		commPort.addDataListener(new SerialDataListenerAvailable(commPort));
		commPort.openPort();
		LOGGER.debug("Using serial port: {}",UART);
		LOGGER.debug("Serial init: OK");
		return commPort;
	}
}

package org.lem.toytrains.config;

import org.lem.toytrains.controller.entity.MultiPlayGameFactory;
import org.lem.toytrains.dcc.DccEventHandler;
import org.lem.toytrains.domain.MultiPlayGame;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.controller.entity.TrackFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

@Configuration
public class MainConfiguration {
	
	@Bean
	public Track track(){
		return TrackFactory.getTrack();
	}
	
	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}
	
	@Bean
	public DccEventHandler dccEventHandler(){return new DccEventHandler();}
	
}

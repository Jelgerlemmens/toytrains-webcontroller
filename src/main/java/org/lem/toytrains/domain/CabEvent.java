package org.lem.toytrains.domain;

public enum CabEvent {
	SPEED,
	LIGHTS,
	SOUND,
	CHANGE_ADDRESS
}
package org.lem.toytrains.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MultiPlayGame {
	
	private List<Player> players;
	private Track track;
	private static final ObjectMapper mapper = new ObjectMapper();
	
	public MultiPlayGame(){} //needed for jackson??
	
	public MultiPlayGame(Track track, Player admin){
		this.players = new ArrayList<>();
		this.players.add(admin);
		this.track = track;
	}
	
	@JsonIgnore
	public void addPlayer(Player player){
		players.add(player);
	}
	
	@JsonIgnore
	public void removePlayer(Player player){
		players.remove(player);
	}
	
	public Player getPlayer(String sessionId){
		Player out = null;
		for(Player player : players){
			if(player.getSessionInformation().getSessionId().equals(sessionId)){
				out = player;
			}
		}
		return out;
	}
	public Track getTrack() {
		return track;
	}
	
	public void setTrack(Track track) {
		this.track = track;
	}
	
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	
	public List<Player> getPlayers(){
		return this.players;
	}
	
	@Override
	public String toString(){
		try{
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		}catch (Exception e){
		
		}
		return super.toString();
	}
}

package org.lem.toytrains.domain;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Turnout {
	
	/*the primary address of the decoder controlling this turnout (0-511)*/
	private int address;
	
	/*the subaddress of the decoder controlling this turnout (0-3)*/
	private int  subAddress;
	
	private boolean turn;
	
	private static final ObjectMapper mapper = new ObjectMapper();

	public Turnout() {}
	
	public Turnout(int address, int subAddress) {
		this.address = address;
		this.subAddress = subAddress;
	}
	
	public int getAddress() {
		return address;
	}

	public boolean isTurn() {
		return turn;
	} 

	public void setTurn(boolean turn) {
		this.turn = turn;
	}
	
	public int getSubAddress() {
		return subAddress;
	}
	
	public void setSubAddress(int subAddress) {
		this.subAddress = subAddress;
	}
	
	@Override
	public String toString(){
		try{
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		}catch (Exception e){
		
		}
		return super.toString();
	}
}

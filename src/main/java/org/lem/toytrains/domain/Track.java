package org.lem.toytrains.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Track {
	
	private final Map<Integer, Cab> cabs = new HashMap<>();
	private final Map<Integer, List<Turnout>> turnouts = new HashMap<>();; //note that turnouts have a primary address and subaddres, the primary address thus has a list of occur turnouts
	private boolean power;
	private boolean debug;
	private TrackEvent trackEvent;
	private String name;
	private static final ObjectMapper mapper = new ObjectMapper();
	
	public Track() {
	}
	
	public Track(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Cab add(Cab cab){
		this.cabs.put(cab.getAddress(), cab);
		return cab;
	}
	
	public Turnout add(Turnout turnout){
		if(Objects.nonNull(this.turnouts.get(turnout.getAddress()))){
			this.turnouts.get(turnout.getAddress()).add(turnout);
		}else{
			List<Turnout> turnoutsSubAddresses = new ArrayList<>();
			turnoutsSubAddresses.add(turnout);
			this.turnouts.put(turnout.getAddress(), turnoutsSubAddresses);
		}
		return turnout;
	}
	
	public void setTrackEvent(TrackEvent trackEvent){
		this.trackEvent = trackEvent;
	}	
	
	public Cab remove(Cab cab) throws Exception {
		if(Objects.nonNull(this.cabs.remove(cab.getAddress()))){
			return cab;
		}else{
			throw new Exception("ERROR deleting CAB from track!");
		}
	}
	
	public Turnout remove(Turnout turnout) throws Exception{
		if(Objects.nonNull(this.turnouts.get(turnout.getAddress()))){
			List<Turnout> turnouts = this.turnouts.get(turnout.getAddress());
			for (Turnout turnout1 : turnouts) {
				if (turnout1.getSubAddress() == turnout.getSubAddress()) {
					turnouts.remove(turnout1);//remove based on address and subaddress
					return turnout1;
				}
			}
			throw new Exception("Turnout sub address does not exist");
		}else{
			throw new Exception("Turnout primary address does not exist");
		}
	}
	
	public Map<Integer, List<Turnout>> getTurnouts(){
		return this.turnouts;
	}
	
	public Map<Integer, Cab> getCabs(){
		return this.cabs;
	}

	public boolean isPower() {
		return power;
	}

	public void setPower(boolean power) {
		this.power = power;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public TrackEvent getTrackEvent() {
		return trackEvent;
	}
	
	@Override
	public String toString(){
		try{
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		}catch (Exception e){
		
		}
		return super.toString();
	}
	
}

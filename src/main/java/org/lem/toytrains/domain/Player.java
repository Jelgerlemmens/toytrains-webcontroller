package org.lem.toytrains.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.session.SessionInformation;

public class Player {
	
	
	private Cab cab;
	private String playerName;
	private SessionInformation sessionInformation;
	private String sessionId;
	private boolean admin;
	private static final ObjectMapper mapper = new ObjectMapper();
	
	public static String PLAYER_PRINCIPAL = "PLAYER";
	
	public Player(String playerName, SessionInformation sessionInformation, boolean admin){
		this.playerName = playerName;
		this.sessionInformation = sessionInformation;
		this.admin = admin;
	}
	
	public Cab getCab() {
		return cab;
	}
	
	public void setCab(Cab cab) {
		this.cab = cab;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public SessionInformation getSessionInformation() {
		return sessionInformation;
	}
	
	public void setSessionInformation(SessionInformation sessionInformation) {
		this.sessionInformation = sessionInformation;
	}
	
	public boolean isAdmin() {
		return admin;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Override
	public String toString(){
		try{
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		}catch (Exception e){
		
		}
		return super.toString();
	}
}

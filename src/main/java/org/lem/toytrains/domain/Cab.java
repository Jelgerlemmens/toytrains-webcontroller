package org.lem.toytrains.domain;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Cab {
	
	private int address;
	private int newDccAddress;
	private int speed;
	private int direction;
	private boolean lights;
	private boolean sound;
	private String imageName;
	private CabEvent cabEvent;
	private static final ObjectMapper mapper = new ObjectMapper();
	private boolean usedInMultiPlay;
	private String playerSessionId;

	public Cab(){}
	public Cab(int address) {
		this.address = address;
	}
	public Cab(int address, String imageName) {
		this.address = address;
		this.imageName = imageName;
	}
	public int getAddress() {
		return this.address;
	}
	
	public void setAddress(int address){
		this.address = address;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public boolean isLights() {
		return lights;
	}

	public void setLights(boolean lights) {
		this.lights = lights;
	}

	public boolean isSound() {
		return sound;
	}

	public void setSound(boolean sound) {
		this.sound = sound;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public CabEvent getCabEvent() {
		return cabEvent;
	}

	public void setCabEvent(CabEvent cabEvent) {
		this.cabEvent = cabEvent;
	}
	
	public void destroyCabEvent() {
		this.cabEvent = null;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	public boolean isUsedInMultiPlay() {
		return usedInMultiPlay;
	}
	
	public void setUsedInMultiPlay(boolean usedInMultiPlay) {
		this.usedInMultiPlay = usedInMultiPlay;
	}
	
	public String getPlayerSessionId() {
		return playerSessionId;
	}
	
	public void setPlayerSessionId(String playerSessionId) {
		this.playerSessionId = playerSessionId;
	}
	
	public int getNewDccAddress() {
		return newDccAddress;
	}
	
	public void setNewDccAddress(int newDccAddress) {
		this.newDccAddress = newDccAddress;
	}
	
	@Override
	public String toString(){
		try{
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		}catch (Exception e){
		
		}
		return super.toString();
	}
}

package org.lem.toytrains.dcc;

import org.lem.toytrains.dcc.serial.SerialCommand;
import org.lem.toytrains.dcc.serial.SerialCommandBuilder;
import org.lem.toytrains.dcc.serial.SerialCommandEvent;
import org.lem.toytrains.dcc.serial.SerialCommandProcessor;
import org.lem.toytrains.dcc.serial.SerialDccWriter;
import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.CabEvent;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.TrackEvent;
import org.lem.toytrains.domain.Turnout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class DccEventHandler {
	
	private static final SerialCommandProcessor SERIAL_COMMAND_PROCESSOR= new SerialCommandProcessor();
	private static final Logger LOGGER = LoggerFactory.getLogger(DccEventHandler.class);
	
	@Autowired
	SerialDccWriter serialDccWriter;
	
	/*
	 * Cab can handle throttle & function events
	 * for every event, send command to serial interface
	 * 
	 */
	public DccEventHandler(){}
	
	public void handle(Cab cab) {
		LOGGER.debug("handling cab: {} with event: {}",cab.getAddress(),cab.getCabEvent());
		SerialCommand sc = toSerialCommand(cab, cab.getCabEvent(), SerialCommandEvent.CAB);
		serialDccWriter.write(SERIAL_COMMAND_PROCESSOR.process(sc));
	}
	
	public void handle(Turnout turnout) {
		LOGGER.debug("handling turnout: {}",turnout.getAddress());
		SerialCommand sc = toSerialCommand(turnout, SerialCommandEvent.TURNOUT);
		serialDccWriter.write(SERIAL_COMMAND_PROCESSOR.process(sc));
	}
	
	public void handle(Track track) {
		LOGGER.debug("handling track: {} with event: {}",track.getName(),track.getTrackEvent());
		SerialCommand sc = toSerialCommand(track, track.getTrackEvent(), SerialCommandEvent.TRACK);
		serialDccWriter.write(SERIAL_COMMAND_PROCESSOR.process(sc));
	}
	
	
	private SerialCommand toSerialCommand(Cab cab, CabEvent t, SerialCommandEvent s) {
		return new SerialCommandBuilder()
				.withCab(cab)
				.withCabEvent(t)
				.withSerialCommandEvent(s)
				.build();				
	}
	
	private static SerialCommand toSerialCommand(Turnout t, SerialCommandEvent s){
		return new SerialCommandBuilder()
				.withTurnout(t)			
				.withSerialCommandEvent(s)
				.build();
	}
	
	private static SerialCommand toSerialCommand(Track track, TrackEvent trackEvent, SerialCommandEvent s){
		return new SerialCommandBuilder()
				.withTrack(track)
				.withTrackEvent(trackEvent)
				.withSerialCommandEvent(s)
				.build();
	}
	
	
	

}

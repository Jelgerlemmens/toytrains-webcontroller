package org.lem.toytrains.dcc.serial;

import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.CabEvent;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.TrackEvent;
import org.lem.toytrains.domain.Turnout;

public class SerialCommandBuilder {

	private Cab cab;
	private Turnout turnout;
	private Track track;
	private CabEvent cabEvent;
	private TrackEvent trackEvent;
	private SerialCommandEvent serialCommandEvent;	
	
    public SerialCommandBuilder withCab(Cab cab) {
        this.cab= cab;
        return this;
    }
    
    public SerialCommandBuilder withTurnout(Turnout turnout) {
        this.turnout = turnout;
        return this;
    }
    
    public SerialCommandBuilder withTrack(Track track) {
        this.track = track;
    return this;
    } 
    
    public SerialCommandBuilder withCabEvent(CabEvent cabEvent) {
        this.cabEvent = cabEvent;
        return this;
    }
      
    public SerialCommandBuilder withTrackEvent(TrackEvent trackEvent) {
        this.trackEvent = trackEvent;
    return this;
    }
    
    public SerialCommandBuilder withSerialCommandEvent(SerialCommandEvent serialCommandEvent) {
        this.serialCommandEvent = serialCommandEvent;
        return this;
    }
    
    public SerialCommand build() {
    	return new SerialCommand(cab, turnout, track, cabEvent, trackEvent, serialCommandEvent);
    }	
}

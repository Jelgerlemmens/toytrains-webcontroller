package org.lem.toytrains.dcc.serial;

public class FunctionByteAdditionValues {
	
	public static final int BASE_BYTE_F0_F4 = 128;
	public static final int BASE_BYTE_F5_F8 = 176;
	public static final int BASE_BYTE_F9_F12 = 160;
	
	//group F0-F4
	public static final int FUNCTION_0 = 16;
	public static final int FUNCTION_1 = 1;
	public static final int FUNCTION_2 = 2;
	public static final int FUNCTION_3 = 4;
	public static final int FUNCTION_4 = 8;
	
	//group F5-F8
	public static final int FUNCTION_5 = 1;
	public static final int FUNCTION_6 = 2;
	public static final int FUNCTION_7 = 4;
	public static final int FUNCTION_8 = 8;
	
	
}

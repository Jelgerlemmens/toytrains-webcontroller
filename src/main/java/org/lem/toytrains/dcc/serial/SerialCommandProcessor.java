package org.lem.toytrains.dcc.serial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.Turnout;

/**
 * Processes a {@link SerialCommand} to an actual command to be sent over serial to the DCCpp base station
 * See: //https://github.com/DccPlusPlus/BaseStation/wiki/Commands-for-DCCpp-BaseStation
 */

public class SerialCommandProcessor {
	
	private static final String DOUBLE_DIGIT_FORMAT = "%02d";
	private static final Logger LOGGER = LoggerFactory.getLogger(SerialCommandProcessor.class);
	
	//one serialCommand only has a single event for e.g. a cab or turnout 
	public String process(SerialCommand serialCommand) {
		LOGGER.debug("Processing serial command for event: {}",serialCommand.toString());
		String processed = null;
		switch(serialCommand.getSerialCommandEvent()) {
			case CAB :
				processed = this.getCabEventString(serialCommand);
				break;
			case TURNOUT :
				processed = this.getTurnoutChangeString(serialCommand.getTurnout());
				break;
			case TRACK :
				processed = this.getTrackEventString(serialCommand);
				break;
			default : 
				break;		
		}
		LOGGER.debug("Processed serial command: {}",processed);
		return processed;			
	}
	
	private String getTrackEventString(SerialCommand serialCommand) {
		String processed = null; 
		switch(serialCommand.getTrackEvent()) {
		case POWER :
			processed = this.getTrackPowerEventString(serialCommand.getTrack());
			break;
		case DEBUG : 
			processed = SerialCommandStrings.TRACKDEBUG;
			break;
		}
		return processed;
	}
	
	private String getCabEventString(SerialCommand serialCommand) {
	String processed;
	switch(serialCommand.getCabEvent()) {
		case SPEED:
			processed = this.getCabSpeedChangeString(serialCommand.getCab());
			break;
		case LIGHTS:
			processed = this.getCabLightsChangeString(serialCommand.getCab());
			break;
		case SOUND:
			processed = this.getCabSoundChangeString(serialCommand.getCab());
			break;
		case CHANGE_ADDRESS:
			processed = this.getCabAddressChangeEventString(serialCommand.getCab());
			break;
		default:
			processed = "";
			break;
	}
			
		return processed;
	}
	
	private String getCabSpeedChangeString(Cab cab) {
		StringBuilder s = new StringBuilder();
		s.append(SerialCommandStrings.OPENCOMMAND);
		s.append(SerialCommandStrings.CAB);
		s.append(SerialCommandStrings.SPACE);
		s.append(SerialCommandStrings.REGISTER);
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT, cab.getAddress()));
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT,cab.getSpeed()));
		s.append(SerialCommandStrings.SPACE);
		s.append(cab.getDirection());
		s.append(SerialCommandStrings.CLOSECOMMAND);
	return s.toString();	
	}
	//<f cabId functionNumberAddition>
	private String getCabLightsChangeString(Cab cab) {
		StringBuilder s = new StringBuilder();
		s.append(SerialCommandStrings.OPENCOMMAND);
		s.append(SerialCommandStrings.FUNCTION);
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT, cab.getAddress()));
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT,this.getF0toF2(cab.isLights())));
		s.append(SerialCommandStrings.CLOSECOMMAND);
	return s.toString();	
	}
	//<f cabId functionNumberAddition>
	private String getCabSoundChangeString(Cab cab) {
		StringBuilder s = new StringBuilder();
		s.append(SerialCommandStrings.OPENCOMMAND);
		s.append(SerialCommandStrings.FUNCTION);
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT, cab.getAddress()));
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT,this.getF3toF4(cab.isSound())));
		s.append(SerialCommandStrings.CLOSECOMMAND);
	return s.toString();	
	}
	
	
	//Base BYTE1 value is 128 functions that need to be turned on are added to the base number
	//toggeling F0 to F3
	private int getF0toF2(boolean state) {
		if(state) {
			return FunctionByteAdditionValues.BASE_BYTE_F0_F4+ FunctionByteAdditionValues.FUNCTION_0+ FunctionByteAdditionValues.FUNCTION_1+ FunctionByteAdditionValues.FUNCTION_2;
		}else {
			return FunctionByteAdditionValues.BASE_BYTE_F0_F4;
		}		
	}
	//toggeling F3 to F4
	private int getF3toF4(boolean state) {
		if(state) {
			return FunctionByteAdditionValues.BASE_BYTE_F0_F4+ FunctionByteAdditionValues.FUNCTION_3+ FunctionByteAdditionValues.FUNCTION_4;
		}else {
			return FunctionByteAdditionValues.BASE_BYTE_F0_F4;
		}		
	}
	//<a id sub state>
	private String getTurnoutChangeString(Turnout turnout) {
		StringBuilder s = new StringBuilder();
		s.append(SerialCommandStrings.OPENCOMMAND);
		s.append(SerialCommandStrings.ASC);
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT, turnout.getAddress())); //dcc address
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT, turnout.getSubAddress())); // dcc sub address
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT,(byte)(turnout.isTurn()?1:0))); //state 1 - 0
		s.append(SerialCommandStrings.CLOSECOMMAND);
	return s.toString();
	}
	
	private String getTrackPowerEventString(Track track) {
		if(track.isPower()) {
			return SerialCommandStrings.OPENCOMMAND + SerialCommandStrings.POWER_ON + SerialCommandStrings.CLOSECOMMAND;
		}else{
			return SerialCommandStrings.OPENCOMMAND + SerialCommandStrings.POWER_OFF + SerialCommandStrings.CLOSECOMMAND;
		}
	}
	
	//change the cab address from cab.getId() (old) to newCabId (new)
	//changes the first CV of the cab < w oldId CV1 newId >
	private String getCabAddressChangeEventString(Cab cab){
		StringBuilder s = new StringBuilder();
		s.append(SerialCommandStrings.OPENCOMMAND);
		s.append(SerialCommandStrings.WRITE);
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT, cab.getAddress()));
		s.append(SerialCommandStrings.SPACE);
		s.append(SerialCommandStrings.CV_ONE);
		s.append(SerialCommandStrings.SPACE);
		s.append(String.format(DOUBLE_DIGIT_FORMAT, cab.getNewDccAddress()));
		s.append(SerialCommandStrings.SPACE);
		s.append(SerialCommandStrings.CLOSECOMMAND);
		return s.toString();
	}
}



package org.lem.toytrains.dcc.serial;

public enum SerialCommandEvent {
	TRACK,
	CAB,
	TURNOUT	
}

package org.lem.toytrains.dcc.serial;

//https://github.com/DccPlusPlus/BaseStation/wiki/Commands-for-DCCpp-BaseStation
public class SerialCommandStrings {
	
	public static final String OPENCOMMAND = "<";
	public static final String CLOSECOMMAND = ">";
	public static final String POWER_ON = "1";
	public static final String POWER_OFF = "0";
	public static final String REGISTER = "1";
	public static final String SPACE = " ";
	public static final String CAB = "t";
	public static final String FUNCTION = "f";
	public static final String ASC = "a";
	public static final String WRITE = "w";
	public static final String CV_ONE = "1";
	public static final String TRACKDEBUG = "<D>";

}




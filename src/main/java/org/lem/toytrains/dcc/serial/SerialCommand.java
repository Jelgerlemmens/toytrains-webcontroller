package org.lem.toytrains.dcc.serial;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.lem.toytrains.domain.Cab;
import org.lem.toytrains.domain.CabEvent;
import org.lem.toytrains.domain.Track;
import org.lem.toytrains.domain.TrackEvent;
import org.lem.toytrains.domain.Turnout;


public class SerialCommand {

	private Cab cab;
	private Turnout turnout;
	private CabEvent cabEvent;
	private SerialCommandEvent serialCommandEvent;
	private Track track;
	private TrackEvent trackEvent;
	private static final ObjectMapper mapper = new ObjectMapper();
	
	public SerialCommand(Cab cab,Turnout turnout, Track track, CabEvent cabEvent, TrackEvent trackEvent, SerialCommandEvent serialCommandEvent) {
		this.setCab(cab);
		this.setTurnout(turnout);
		this.setTrack(track);
		this.setCabEvent(cabEvent);
		this.setTrackEvent(trackEvent);		
		this.setSerialCommandEvent(serialCommandEvent);
				
	}

	public Cab getCab() {
		return cab;
	}

	public void setCab(Cab cab) {
		this.cab = cab;
	}

	public Turnout getTurnout() {
		return turnout;
	}

	public void setTurnout(Turnout turnout) {
		this.turnout = turnout;
	}

	public CabEvent getCabEvent() {
		return cabEvent;
	}

	public void setCabEvent(CabEvent cabEvent) {
		this.cabEvent = cabEvent;
	}

	public SerialCommandEvent getSerialCommandEvent() {
		return serialCommandEvent;
	}

	public void setSerialCommandEvent(SerialCommandEvent serialCommandEvent) {
		this.serialCommandEvent = serialCommandEvent;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public TrackEvent getTrackEvent() {
		return trackEvent;
	}

	public void setTrackEvent(TrackEvent trackEvent) {
		this.trackEvent = trackEvent;
	}
	
	@Override
	public String toString(){
		try{
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		}catch (Exception e){
		
		}
		return super.toString();
	}
		
}

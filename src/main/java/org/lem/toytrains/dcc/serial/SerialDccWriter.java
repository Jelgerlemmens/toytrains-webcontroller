package org.lem.toytrains.dcc.serial;

import com.fazecast.jSerialComm.SerialPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SerialDccWriter {
	
	@Autowired
	SerialPort serialPort;

	private static final Logger LOGGER = LoggerFactory.getLogger(SerialDccWriter.class);

	public SerialDccWriter() {
	}
	
	public void write(String s) {
		try {
			if(serialPort.isOpen()){
				serialPort.writeBytes(s.getBytes(),s.getBytes().length);
			}
			LOGGER.debug("Serial wrote: "+s);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
}

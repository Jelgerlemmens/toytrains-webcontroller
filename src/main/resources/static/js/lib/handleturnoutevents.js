function getTurnoutFromModal(){
	var turnout = new Turnout($('#turnoutIdModal').val(),$('#turnoutSubIdModal').val());
	return turnout;
}

function deleteTurnoutInsetFromTable(turnout){
	$('.turnoutInset').each(function(index){
		var concatId = turnout.address +"-"+turnout.subAddress;
		if($( this ).attr('id') == concatId){
			$( this ).remove();
		}
	})
}

//add a control inset for the added cab
function createTurnoutInsetInTable(data){
	var turnoutInsetMarkup = getTurnoutInsetMarkup(data);
	$('#turnoutTable > tbody').append(turnoutInsetMarkup);
}


function addTurnout() {
	var turnout = getTurnoutFromModal();
	$.ajax({
		type: 'POST',
		url: '/addturnout',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(turnout),
		success: function (data) {
			createTurnoutInsetInTable(data);
		},
		error: function(data){
			console.log('Turnout could not be added')
		}
    })
    $('#addTurnoutModal').modal('hide');
}

function deleteTurnout(address, subAddress){
	var turnout = new Turnout(address,subAddress);
 	$.ajax({
		type: 'POST',
		url: '/deleteturnout',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(turnout),
		success: function (data) {
			deleteTurnoutInsetFromTable(data);
		}
     })
}

function setTurnoutToggle(address, subAddress){
	var turnout = new Turnout(address, subAddress);
 	$.ajax({
		type: 'POST',
		url: '/toggleturnout',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(turnout),
     })
}

function handleTurnoutToggleEvent(){
	$(document.body).on('click','.insetTurnoutToggleButton',function(event){
		var unsplitId = $(this).closest('.turnoutInset').attr('id')
		var arr = unsplitId.split("-");
		setTurnoutToggle(arr[0],arr[1]);
    });
}

function handleTurnoutDeleteEvent(){
	$(document.body).on('click','.insetTurnoutDeleteButton',function(event){
		var unsplitId = $(this).closest('.turnoutInset').attr('id')
		var arr = unsplitId.split("-");
		deleteTurnout(arr[0],arr[1]);
    });
}

function getCabFromModal(){
	var cab = new Cab($('#cabIdModal').val());
	cab.imageName  = $( '#my-icon-select > div > div.selected-icon > img' ).attr('src');
	return cab;
}

//add a control inset for the added cab
function createCabInsetInTable(data){
	var addedCab = data;
	var cabInsetMarkup = getCabInsetMarkup(addedCab);
	$('#cabTable > tbody').append(cabInsetMarkup);
}

function deleteCabInsetFromTable(cab){
	$('.cabInset').each(function(index){
		if($( this ).attr('id') == cab.address){
			$( this ).remove();
		}
	})
}

function updateCabInsetInTable(oldCabaddress, newCabaddress, cab){
	var oldCab = new Cab(oldCabaddress);
	deleteCabInsetFromTable(oldCab);
	createCabInsetInTable(cab);
}

function toggleLightsButton(cab){
	if(cab.lights){
		$('#'+cab.address).find('.insetCabLightButton').addClass("btn-warning");
	}else{
		$('#'+cab.address).find('.insetCabLightButton').removeClass("btn-warning");
	}
}

function fillCabDropDown(){
	$('#changeCabIdModalDropDown').html('Cabs');
	$('#changeCabIdModalDropDownCabsList').empty();
	var cabs = getAllCabsOnTrack();
	for(var i =0; i < cabs.length; i++){
		$('#changeCabIdModalDropDownCabsList').append(
 			"<button class='dropdown-item cabDropDownItem' type='button' id='"+cabs[i].address+"'>"+cabs[i].address+"</button>"
		)
	}
	bindClickFunctionToDropdownItems();
}



//----------------------AJAX----------------------
function addCab() {
	var cab = getCabFromModal();
	$.ajax({
		type: 'POST',
		url: '/addcab',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(cab),
		success: function (data) {
			createCabInsetInTable(data);
		},
		error: function(data){
			console.log('Cab could not be added, address already exists')
		}
    })
    $('#addCabModal').modal('hide');
}

function getAllCabsOnTrack(){
	var cabsArray = null;
	$.ajax({
		type: 'GET',
		url: '/allcabsontrack',
		async: false,
		success: function (data) {
			console.log(data);
			cabsArray = data;
		}
	 })
	 return cabsArray;
}


function deleteCab(cabaddress){
	var cab = new Cab(cabaddress);
 	$.ajax({
		type: 'POST',
		url: '/deletecab',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(cab),
		success: function (data) {
			deleteCabInsetFromTable(data);
		}
     })
}

function setCabSpeed(cabaddress, throttleValue){
	var cab = new Cab(cabaddress);
	cab.speed = throttleValue;
 	$.ajax({
		type: 'POST',
		url: '/setcabspeed',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(cab),
     })
}

function setCabLights(cabaddress){
	var cab = new Cab(cabaddress);
 	$.ajax({
		type: 'POST',
		url: '/setcablights',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(cab),
		success: function (data) {
			toggleLightsButton(data);
		}
     })
}

function setCabSound(cabaddress){
	var cab = new Cab(cabaddress);
 	$.ajax({
		type: 'POST',
		url: '/setcabsound',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(cab),
     })
}

function changeCabId(){
	var newCabaddress = $('#changeCabIdModalNewCabId').val();
	var oldCabaddress = $('#changeCabIdModalDropDown').val();

	$.ajax({
		type: 'GET',
		url: '/setcabaddress',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: {
			oldCabaddress : oldCabaddress,
			newCabaddress : newCabaddress
		},
		success: function (data) {
			updateCabInsetInTable(oldCabaddress, newCabaddress, data)
		}
    })
    $('#changeCabIdModal').modal('hide');
}


//------------ Event handling -------------------

function handleCabDeleteEvent(){
	$(document.body).on('click','.insetCabDeleteButton',function(event){
		var cabaddress = $(this).closest('.cabInset').attr('id')
		deleteCab(cabaddress);
    });
}

function handleCabSpeedEvent(){
	$(document.body).on('input','.insetCabThrottle',function(event){
		var cabaddress = $(this).closest('.cabInset').attr('id')
		var throttleValue = $(this).val();
		setCabSpeed(cabaddress,throttleValue);
    });
}


function handleCabPanicEvent(){
	$(document.body).on('click','.insetCabPanicButton',function(event){
		var cabaddress = $(this).closest('.cabInset').attr('id')
		$(this).closest('.cabInset').find('.insetCabThrottle').val(0);
		setCabSpeed(cabaddress,0);
    });
}

function handleCabLightEvent(){
	$(document.body).on('click','.insetCabLightButton',function(event){
		var cabaddress = $(this).closest('.cabInset').attr('id')
		setCabLights(cabaddress);
    });
}

function handleCabHornEvent(){
	$(document.body).on('click','.insetCabHornButton',function(event){
		var cabaddress = $(this).closest('.cabInset').attr('id')
		setCabSound(cabaddress);
    });
}

function bindClickFunctionToDropdownItems(){
	$(document.body).on('click', '.cabDropDownItem',function(event){
		var newVal = $(this).attr('id');
		$('#changeCabIdModalDropDown').prop('value',newVal);
		$('#changeCabIdModalDropDown').html(newVal);
	})
}

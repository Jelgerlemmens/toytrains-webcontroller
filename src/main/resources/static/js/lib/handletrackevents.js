function toggleTrackPower(){
	$.ajax({
		type: 'GET',
		url: '/toggletrackpower',
		success: function (data) {
			setTrackPowerButtonState(data);
		}
	 })
}

function setTrackPowerButtonState(state){
	if(state){
		$('#trackPowerButton').removeClass("btn-danger");
		$('#trackPowerButton').addClass("btn-success");
	}else{
		$('#trackPowerButton').removeClass("btn-success");
        $('#trackPowerButton').addClass("btn-danger");
	}
}


function saveTrack(){
	$.ajax({
		type: 'GET',
		url: '/savetrack',
		success: function (data) {
			showTrackSaveModal(data);
        }
	 })
}

function showTrackSaveModal(data){
	$('#trackSaveState').empty();
	if(data){
		$('#trackSaveState').append("Track save successful")
	}else{
		$('#trackSaveState').append("Track save failed")
	}
	$('#trackSaveModal').modal('toggle');
}
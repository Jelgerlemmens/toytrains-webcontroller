
var turnouts = Object.entries(turnoutsMap);
var playerData = player;

$(document).ready(function() {
	setPlayerName(playerData)
	createCabTable(playerData);
	createTurnoutTable(turnouts);
	bindCabEventHandling();
	bindTurnoutEventHandling();
 	pageRefresh();
});

//------------Cab Table------------
function createCabTable(playerData){
	var cabTableMarkup = "<table class='table' id='cabTable'><thead><tr><th scope='col'><h4>Cabs</h4></th></tr></thead><tbody></tbody></table>";
    $('#cabTableContainer').append(cabTableMarkup);
	var cabInsetMarkup =  getCabInsetMarkupForPlayerView(playerData.cab);
	$('#cabTable > tbody').append(cabInsetMarkup);
}

function setPlayerName(playerData){
	$('#playerNameHeader').text(playerData.playerName);
}

//------------Turnout Table------------
function createTurnoutTable(){
	var turnoutTableMarkup = "<table class='table ' id='turnoutTable'><thead><tr><th scope='col'><h4>Turnouts</h4></th></tr></thead><tbody></tbody></table>";
	$('#turnoutTableContainer').append(turnoutTableMarkup);
	for(i = 0 ; i < turnoutsArray.length; i++){ //loops trough the available addresses of turnouts
		var addressArray = turnoutsArray[i];
		for(j = 1 ; j < addressArray.length; j++){ //loops trough the available subaddresses (and thus turnouts)
			var subAddrArray = addressArray[j];
			for(k = 0 ; k < subAddrArray.length ; k++){
				 var turnoutInsetMarkup = getTurnoutInsetMarkup(subAddrArray[k])
				$('#turnoutTable > tbody').append(turnoutInsetMarkup);
			}
		}
	}
}


//function createTurnoutInsetMarkupOnStart(turnout){
//	return getTurnoutInsetMarkupForPlayerView(turnout[1]);
//}

function logOut(){
	$.ajax({
		type: 'GET',
		url: '/logoutplayer',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: {
			playerName : player.playerName,
			sessionId : player.sessionId
		},
		success: function (data) {
			$.removeCookie('JSESSIONID');
			//$.removeCookie('JSESSIONID', { path: '/' });
			window.location.replace("/");
		}
    })

}


//refresh page from server every 30 sec
function pageRefresh(){
	setInterval(function(){
		location.reload(true);
	},1200000);
}

//------------Cab event handling binding------------
function bindCabEventHandling(){
	handleCabSpeedEvent();
	handleCabPanicEvent();
	handleCabLightEvent();
	handleCabHornEvent();
}

function bindTurnoutEventHandling(){
	handleTurnoutToggleEvent();
	handleTurnoutDeleteEvent();
}
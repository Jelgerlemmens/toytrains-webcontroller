$(document).ready(function() {
       toggleSinglePlayerButton();
})

function toggleSinglePlayerButton(){
	if(isMultiPlayerGameActive()){
		$('#singlePlayerButton').addClass('btnLargeDisabled');
		$('#singlePlayerButton').removeClass('btnLarge');
		$('#singlePlayerButton').attr("disabled", true);
	}else{
		$('#singlePlayerButton').removeClass('btnLargeDisabled');
		$('#singlePlayerButton').addClass('btnLarge');
    	$('#singlePlayerButton').attr("disabled", false);
	}
}

function isMultiPlayerGameActive(){
	if(multiplaygame == null){
		return false;
	}else{
		return true;
	}
}

function singlePlayerClick(){
	window.location.replace("/tosingleplay");
}

function multiPlayerClick(){
	window.location.replace("/tomultiplay");
}

var iconSelect;
var cabsArray;
var turnoutsArray;


$(document).ready(function() {
	cabsArray = Object.entries(cabsMap);
	turnoutsArray = Object.entries(turnoutsMap);
	loadIconselect();
	createCabTable();
	createTurnoutTable();
	setTrackPowerButtonState(trackPower);
 	bindCabEventHandling();
 	bindTurnoutEventHandling();
 	pageRefresh();
});

function loadIconselect(){
//	iconSelect = new IconSelect("my-icon-select");
	 iconSelect = new IconSelect("my-icon-select",
                    {'selectedIconWidth':48,
                    'selectedIconHeight':48,
                    'selectedBoxPadding':1,
                    'iconsWidth':40,
                    'iconsHeight':40,
                    'boxIconSpace':1,
                    'vectoralIconNumber':4,
                    'horizontalIconNumber':4});
	var icons = [];
	icons.push({'iconFilePath':'images/train_icons/train_icon_1.png', 'iconValue':'1'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_2.png', 'iconValue':'2'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_3.png', 'iconValue':'3'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_4.png', 'iconValue':'4'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_5.png', 'iconValue':'5'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_6.png', 'iconValue':'6'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_7.png', 'iconValue':'7'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_8.png', 'iconValue':'8'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_9.png', 'iconValue':'9'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_10.png', 'iconValue':'10'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_11.png', 'iconValue':'11'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_12.png', 'iconValue':'12'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_13.png', 'iconValue':'13'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_14.png', 'iconValue':'14'});
	iconSelect.refresh(icons);
}

//------------Cab Table------------
function createCabTable(){
	var cabTableMarkup = "<table class='table' id='cabTable'><thead><tr><th scope='col'><h4>Cabs</h4></th></tr></thead><tbody></tbody></table>";
    $('#cabTableContainer').append(cabTableMarkup);
    for(i =0; i < cabsArray.length; i++){
		var cabInsetMarkup = createCabInsetMarkupOnStart(cabsArray[i]);
		$('#cabTable > tbody').append(cabInsetMarkup);
    }
}

function createCabInsetMarkupOnStart(cab){
	return getCabInsetMarkup(cab[1]);
}

//------------Turnout Table------------
function createTurnoutTable(){
	var turnoutTableMarkup = "<table class='table ' id='turnoutTable'><thead><tr><th scope='col'><h4>Turnouts</h4></th></tr></thead><tbody></tbody></table>";
	$('#turnoutTableContainer').append(turnoutTableMarkup);
	for(i = 0 ; i < turnoutsArray.length; i++){ //loops trough the available addresses of turnouts
		var addressArray = turnoutsArray[i];
		for(j = 1 ; j < addressArray.length; j++){ //loops trough the available subaddresses (and thus turnouts)
			var subAddrArray = addressArray[j];
			for(k = 0 ; k < subAddrArray.length ; k++){
				 var turnoutInsetMarkup = getTurnoutInsetMarkup(subAddrArray[k])
				$('#turnoutTable > tbody').append(turnoutInsetMarkup);
			}
		}
	}
}


//function createTurnoutInsetMarkupOnStart(turnout){
//	return getTurnoutInsetMarkup(turnout[1]);
//}

//------------Cab event handling binding------------
function bindCabEventHandling(){
	handleCabDeleteEvent();
	handleCabSpeedEvent();
	handleCabPanicEvent();
	handleCabLightEvent();
	handleCabHornEvent();
}

function bindTurnoutEventHandling(){
	handleTurnoutToggleEvent();
	handleTurnoutDeleteEvent();
}

//refresh page from server every 30 sec
function pageRefresh(){
	setInterval(function(){
		location.reload(true);
	},1200000);
}

function endGame(){
	//var foo = $.cookie("foo").
	var adminPlayerName = admin.playerName;
	var sessionId = admin.sessionId;
	$.ajax({
		type: 'GET',
		url: '/endmultiplayergame',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: {
			adminPlayerName : adminPlayerName,
			sessionId : sessionId
		},
		success: function (data) {
			window.location.replace("/");
		}
	})
}

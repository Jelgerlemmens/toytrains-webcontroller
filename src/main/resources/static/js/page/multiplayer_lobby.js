var cabs = Object.entries(cabsMap);
var iconSelect;


$(document).ready(function() {
	disableButtons(multiplaygame);
	loadIconselect();
	loadCabs();
});

function disableButtons(multiplaygame){
	if(multiplaygame == null){
		$('#joinMultiplayerGameBtn').addClass('btnLargeDisabled');
		$('#joinMultiplayerGameBtn').removeClass('btnLarge');
		$('#joinMultiplayerGameBtn').attr("disabled", true);
	}else{
		$('#createMultiplayerGameBtn').addClass('btnLargeDisabled');
		$('#createMultiplayerGameBtn').removeClass('btnLarge');
		$('#createMultiplayerGameBtn').attr("disabled", true);
	}
}

function loadCabs(){
	for(var i = 0; i < cabs.length ; i++ ){
		if(!cabs[i][1].usedInMultiPlay){
			$('#playerCabId').append(getCabIdOption(cabs[i][1].id));
        }
	}
}

function getCabIdOption(id){
	var optionMarkup ="<option>"+id+"</option>"
	return optionMarkup;
}

function loadIconselect(){
	 iconSelect = new IconSelect("my-icon-select",
                    {'selectedIconWidth':48,
                    'selectedIconHeight':48,
                    'selectedBoxPadding':1,
                    'iconsWidth':40,
                    'iconsHeight':40,
                    'boxIconSpace':1,
                    'vectoralIconNumber':4,
                    'horizontalIconNumber':4});
	var icons = [];
	icons.push({'iconFilePath':'images/train_icons/train_icon_1.png', 'iconValue':'1'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_2.png', 'iconValue':'2'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_3.png', 'iconValue':'3'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_4.png', 'iconValue':'4'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_5.png', 'iconValue':'5'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_6.png', 'iconValue':'6'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_7.png', 'iconValue':'7'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_8.png', 'iconValue':'8'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_9.png', 'iconValue':'9'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_10.png', 'iconValue':'10'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_11.png', 'iconValue':'11'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_12.png', 'iconValue':'12'});
	icons.push({'iconFilePath':'images/train_icons/train_icon_13.png', 'iconValue':'13'});
	iconSelect.refresh(icons);
}

function clickJoinNewPlayer(){
	var cabCount = $('#playerCabId option').length; //available cabs..
	if(cabCount > 0){
		$('#newPlayerJoinModal').modal('show');
	}else{
		alert("Cannot join game, no cabs available!");
		$('#newPlayerJoinModal').modal('hide');
	}
}

function startNewMultiPlayerGame(){
	var adminPlayerName = $('#adminPlayerName').val();
	$.ajax({
		type: 'GET',
		url: '/newmultiplayergame',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: {
			adminPlayerName : adminPlayerName
		},
		success: function (data) {
			$('#newMultiPlayerGameModal').modal('hide');
			console.log(data);
			gotoGame(data.players[0]);
		}
	})
}

function gotoGame(player){
	if(player.admin == true){
		window.location.replace("/adminplayerview");
	}else{
		window.location.replace("/playerview");
	}
}

function joinGame(){
var player = getPlayerFromView();
	$.ajax({
		type: 'POST',
		url: '/joinmultiplayergame',
		dataType: 'json',
		contentType: 'application/json',
		async: false,
		data: JSON.stringify(player),
		success: function (data) {
			gotoGame(data);
		},
		error: function(data){
			console.log("fail");
		}
	})
}


function getPlayerFromView(){
	var name = $('#multiPlayerPlayerName').val();
	var cabId = $('#playerCabId').val();
	var cabimageName  = $( '#my-icon-select > div > div.selected-icon > img' ).attr('src');
	var jsId = getJSessionId();
	var cab = new Cab(cabId);
	cab.imageName = cabimageName;
	cab.usedInMultiPlay = true;
	var player = new Player(name);
	player.cab = cab;
	player.sessionId = jsId;
	return player;
}

function getJSessionId(){
    var jsId = document.cookie.match(/JSESSIONID=[^;]+/);
    if(jsId != null) {
        if (jsId instanceof Array)
            jsId = jsId[0].substring(11);
        else
            jsId = jsId.substring(11);
    }
    return jsId;
}

function goHome(){
	window.location.replace("/");
}
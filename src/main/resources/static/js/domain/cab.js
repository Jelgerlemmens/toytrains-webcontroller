//var cab = { "id" : 10, "speed" : 0, "direction" : 0, "lights" : false, "sound" : false, "imageName" : null, "cabEvent" : null };

class Cab {
	constructor(address){
		this.address = address;
	}
	address = null;
	speed = 0;
	direction = 0;
	lights = false;
	sound = false;
	imageName = null;
	cabEvent = null;
	usedInMultiPlay = false;
	new_dcc_address = null;
}